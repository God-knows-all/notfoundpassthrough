# Not Found Passthrough

Formerly Redirect 404, this module allows you to specify domains that should
be attempted as a fallback to find content when a "404 page not found" error
is encountered on your site.

This module is useful when you are doing a gradual migration to Drupal from
another site (Drupal-based or not).

## Description

Let's say you have a legacy system which contains most of your web presence 
and decide to migrate to Drupal, starting with the home page, but didn't 
want to orphan the legacy site. 

So, URL such as http://www.example.com/department would stop working 
and would return 404 to users visiting the site.

With mod_rewrite in Apache, it is possible to redirect 404 to an external URL, 
but it does not preserve the full request path nor support multiple 
destination servers.

If you specify legacy1.example.com and legacy2.example.com as your possible 
destinations for your Drupal site on www.example.com and a user visited 
http://www.example.com/department, which hadn't existed, Not Found Passthrough 
would then try legacy1.example.com/department if found, the user would be 
redirected. If not found, legacy2.example.com/department would be tried, 
and if found, the user would be redirected to that URL instead. If still 
not found, a custom 404 error page may be specified or a user message 
defined. You could also specify a site search URL as an option, so if you 
try to go to www.example.com/department and no servers are found that know 
what to do with the request, the user is taken to the search results page 
for department (example: www.example.com/search/department)

# Incompatibilities

- Requests which are handled with the Fast 404 module are not be handled by 
Redirects 404. Fast 404 "runs first" and the handles the requests on its own.

<!-- writeme -->
Notfoundpassthrough
===================



 * https://www.drupal.org/project/notfoundpassthrough
 * Issues: https://www.drupal.org/project/issues/notfoundpassthrough
 * Package name: drupal/notfoundpassthrough


### Maintainers

 * Benjamin Melançon - https://agaric.coop/mlncn
 * David Valdez - https://agaric.coop/gnuget
 * Vlad Pavlovic - https://www.drupal.org/u/vladpavlovic
 * Lucian NEAG - https://www.drupal.org/u/luxian
 * See contributors - https://www.drupal.org/node/3170743/committers


### Requirements

No dependencies.


### License

GPL-2.0+

<!-- endwriteme -->
