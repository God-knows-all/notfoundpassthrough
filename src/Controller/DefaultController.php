<?php
/**
 * @file
 * Contains \Drupal\notfoundpassthrough\Controller\DefaultController.
 */

namespace Drupal\notfoundpassthrough\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default controller for the notfoundpassthrough module.
 */
class DefaultController extends ControllerBase {

  public function _notfoundpassthrough_title() {
    return $this->config('notfoundpassthrough.settings')->get('title');
  }

  public function notfoundpassthrough(Request $request) {
    global $is_https;
    // Check the protocol in case it's not defined.
    $protocol = $is_https ? 'https://' : 'http://';
    $servers = explode("\n", $this->config('notfoundpassthrough.settings')->get('servers'));
    // Initialize the variables.
    $new_status = '';
    $new_status_code = 0;
    $new_path = '';
    $save_redirect = $this->config('notfoundpassthrough.settings')->get('save_redirect');
    $statuses = _notfoundpassthrough_redirect_options();
    $redirect_code = $this->config('notfoundpassthrough.settings')->get('redirect_code');
    $force_status = $this->config('notfoundpassthrough.settings')->get('force_redirect_code');
    $code = NULL;
    if ($request->getRequestUri() !== '/notfoundpassthrough') {
      foreach ($servers as $server) {
        $path = (FALSE === strpos($server, '://')) ? $protocol . trim($server) : trim($server);
        if (FALSE === strpos($path, '[request_uri]')) {
          $path .= $request->getRequestUri();
        }
        else {
          $path = str_replace('/[request_uri]', $request->getRequestUri(), $path);
        }
        $client = \Drupal::httpClient('',
          [
            'request.options' => [
              'timeout' => 3,
              'connection_timeout' => 2,
            ],
          ]);
        try {
          $response = $client->head($path);
          $code = $response->getStatusCode();
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
          watchdog_exception('notfoundpassthrough', $e, 'Caught response: ' . $e->getResponse()->getStatusCode());
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
          watchdog_exception('notfoundpassthrough', $e);
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
          watchdog_exception('notfoundpassthrough', $e);
        }
        if ($code >= 200 && $code <= 250) {
          // We found the actual document on this server, set to 302 Found.
          $new_status_code = $redirect_code;
          // TODO figure out how to get Guzzle to give us back the URL/path
          // that we finally ended up on.  There doesn't seem to be a way so
          // we would have to send the Max-Forwards request-header and collect
          // each path as we go.

          $new_path = $path;
        }
        elseif ($code >= 300 && $code <= 305) {
          // This is never going to happen...  if we decide we care...
          // TODO figure out how to get Guzzle to give us back the URL/path
          // that we finally ended up on.  There doesn't seem to be a way so
          // we would have to send the Max-Forwards request-header and collect
          // each path as we go.
          // For now, knowing that the path we started with ultimately ended up
          // with a 200 status code (captured in above) is good enough.

          // No resource, but found another redirect, so follow the redirect.
          $new_status_code = !$force_status ? $code : $redirect_code;
          // $new_path = (isset($header_info->redirect_url)) ? ($header_info->redirect_url) : $path;
        }
        // Anything other than 200s or 300s and the path wasn't found, so we
        // have nothing to redirect to.
      }
    }
    // Generate the redirect code based on redirect code.
    if ($new_status_code) {
      $new_status = $statuses[$new_status_code];
    }
    // If there is no status/path to use, see if we are searching.
    if (!$new_status && !$new_path) {
      // TODO Integrate with Search 404 instead of doing this ourselves.
      $search_path = $this->config('notfoundpassthrough.settings')->get('search');
      if ($search_path) {
        drupal_set_message(t('The page you were trying to navigate to could not be found. Below you will see search results which may help you find what you are looking for.'));
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = $search_path . '/' . trim(str_replace('/', ' ', $request->getRequestUri()));
        // We do not save a search redirect.
        $save_redirect = FALSE;
      }
    }
    // If there is no status/path to use, see if a simple redirect is specified.
    if (!$new_status && !$new_path) {
      $redirect_path = $this->config('notfoundpassthrough.settings')->get('redirect');
      if ($redirect_path) {
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = str_replace('/[request_uri]', $request->getRequestUri(), $redirect_path);
      }
    }
    // If there is still no status/path to use, fall back to set 404 path.
    if (!$new_status && !$new_path) {
      $redirect_path = $this->config('notfoundpassthrough.settings')->get('site_404');
      if ($redirect_path) {
        $new_status = '302 Found';
        $new_status_code = 302;
        $new_path = url($redirect_path);
        // We do not save a redirect path redirect.
        $save_redirect = FALSE;
      }
    }
    // If there is a status and a path, we go there.
    if ($new_status && $new_path) {
      // TODO after Redirect module gets some developer documentation
      // https://www.drupal.org/project/redirect/issues/3001965
      /*
      if (\Drupal::moduleHandler()->moduleExists('redirect') && $save_redirect) {
        // Save to Redirect module system.
        $redirect = new stdClass();
        redirect_object_prepare($redirect, [
          'source' => ltrim($request->getRequestUri(), '/'),
          'redirect' => $new_path,
          'status_code' => $new_status_code,
        ]);
        redirect_save($redirect);
      }
      */
      // Remove the destination because Drupal 8 is trying to drive its
      // developers to suicide.  There is a proposed fix in this issue:
      // https://www.drupal.org/project/drupal/issues/2950883
      \Drupal::request()->query->remove('destination');
      // TODO probably only do the above and below when actually redirecting to
      // a remote site, not doing one of the in-site redirects that's also
      // possible.
      \Drupal::logger('notfoundpassthrough')->notice('Redirected to ' . $new_path);
      return new TrustedRedirectResponse($new_path, $new_status_code);
    }
    $build = [
      '#markup' => $this->config('notfoundpassthrough.settings')->get('content'),
    ];
    return $build;
  }

}
